import tensorflow as tf
import os
from config import FLAGS
import numpy as np
os.environ["CUDA_VISIBLE_DEVICES"]=str(FLAGS.gpu)
from config import FLAGS
import data_utils
word2ix = data_utils.create_vocab(annotation_prefix='./Dense_VTT/annotation')

import model_zoo
import collections
import json
from sklearn.model_selection import KFold

if __name__=='__main__':

    # # 10-fold CV
    # kfold = KFold(10, True, 1)
    # K = FLAGS.K
    #
    # data = collections.OrderedDict(json.load(file('./Dense_VTT/annotation/train.json'))).items()
    # train_ixs, valid_ixs = list(kfold.split(data))[K]
    # train_data, valid_data = np.array(data)[train_ixs].tolist(), np.array(data)[valid_ixs].tolist()

    train_data = collections.OrderedDict(json.load(file('./Dense_VTT/annotation/train.json'))).items()
    test_data = collections.OrderedDict(json.load(file('./Dense_VTT/annotation/val_1.json'))).items()

    # batchers for each dataset
    train_batcher = data_utils.DatasetBatcher(data=train_data, feats_dir=os.path.join(FLAGS.feats_home, 'train'), type='train', word2ix=word2ix, batch_size=FLAGS.batch_size)
    #valid_batcher = data_utils.DatasetBatcher(data=valid_data, feats_dir=os.path.join(FLAGS.feats_home, 'train'), type='valid', word2ix=word2ix)
    test_batcher = data_utils.DatasetBatcher(data=test_data, feats_dir=os.path.join(FLAGS.feats_home, 'val_1'), type='test', word2ix=word2ix, batch_size=FLAGS.batch_size)

    if FLAGS.mode=='test_DNC':
        ph = dict(n_events=tf.placeholder(tf.int32, name='n_events'),
                  multi_input_data=tf.placeholder(tf.float32, shape=[None, FLAGS.batch_size, None, FLAGS.input_size],
                                                  name='multi_input_data'),
                  dnc_output_mask=tf.placeholder(tf.float32, shape=[None, FLAGS.batch_size, None],
                                                 name='dnc_output_mask'),
                  )
        m = model_zoo.DNC_C3D(ph, is_train=False)
        m.test(test_batcher)