#-*- coding: utf-8 -*-
import numpy as np
import cv2
import os
import json
import collections
from config import FLAGS
from nltk.tokenize.moses import MosesTokenizer, MosesDetokenizer

# init tokenizer
t = MosesTokenizer()

# init detokenizer
d = MosesDetokenizer()

def mean_subtract_and_center_crop(frame, mean_frame):
    # mean frame from dataset
    mean_frame_height, mean_frame_width, _ = mean_frame.shape

    # mean subtract
    frame = cv2.resize(frame, (mean_frame_width,mean_frame_height))-mean_frame

    # center-crop, (w,h)=(112,112)
    res = frame[8:120, 30:142, :]

    return res

def create_vocab(annotation_prefix='./Dense_VTT/annotation'):
    all_json_files = map(lambda _type: os.path.join(annotation_prefix, _type) + '.json', ['train', 'val_1'])
    all_data = {}
    for f in all_json_files:
        with open(f, 'rb') as fp:
            all_data.update(json.load(fp))
    all_data = collections.OrderedDict(all_data).items()

    all_words = []
    for _, anno in all_data:
        for cap in anno['sentences']:
            s = cap.strip().lower()
            all_words += t.tokenize(s)

    freq = collections.Counter(all_words).most_common()
    word2ix = {'<PAD>': 0, '<GO>': 1, '<EOS>': 2}
    word2ix.update(dict(zip(zip(*freq)[0], range(3, len(freq) + 3))))

    json.dump(word2ix, file('word2ix.json', 'w'))

    return word2ix

def postprocess_sent(sent):
    return ' '.join(d.tokenize(sent.split()))

class FrameBatcher:
    def __init__(self, data, type, thread_index, batch_size=1):
        self.data = data
        self.type= type
        self.thread_index = thread_index
        self.video_prefix = FLAGS.video_prefix
        self.batch_size = batch_size
        self.Batch = collections.namedtuple('Batch',['vid','frames','timestamps'])
        self.start = 0
        self.epoch = 0

    def prepare_feed_data(self, session, feats_sequence, tf_video_clip):
        _input_data_list = []

        batch = self.next_batch()

        _input_data_list.append(batch.vid)

        n_events = np.array(batch.timestamps).shape[1]

        # for broadcasting
        frame_arr = np.array(batch.frames[0])
        for j in range(n_events):
            try:
                cur_feats = session.run(feats_sequence,
                                        feed_dict={tf_video_clip: [frame_arr[j]]})
            except:
                # some data is trash...fuck
                print 'fuck'
                continue

            if cur_feats.shape[1]==0:
                print 'catch'

            _input_data = cur_feats
            _input_data_list.append(_input_data)

        return _input_data_list

    def get_frames_data(self, cap, timestamps):
        crop_mean = np.load('train01_16_128_171_mean.npy').transpose(1, 2, 3, 0)  # (16, 128, 171, 3)
        frame_data = []

        for start, end in timestamps:
            cap.set(cv2.CAP_PROP_POS_MSEC, start * 1000)
            event_frames = []
            t = 0
            cnt = 0
            sample_rate = 10
            if ((end-start)*cap.get(cv2.CAP_PROP_FPS))/sample_rate < 2*FLAGS.DELTA:
                sample_rate = 1

            while cap.get(cv2.CAP_PROP_POS_MSEC) < end * 1000:
                ret, frame = cap.read()
                if ret:
                    if cnt % sample_rate == 0:
                        if FLAGS.pretrained=='sports1m':
                            frame = mean_subtract_and_center_crop(frame, crop_mean[t % FLAGS.DELTA])
                        elif FLAGS.pretrained=='kinetic':
                            frame = cv2.resize(frame, (FLAGS.IMG_SIZE,FLAGS.IMG_SIZE))
                            frame = frame.astype(np.float32)
                        event_frames.append(frame)
                        t += 1
                    cnt += 1
                else:
                    break

            frame_data.append(event_frames)

        return frame_data

    def next_batch(self):
        cur_batch = self.Batch(vid=[], frames=[], timestamps=[])
        batch_sample = self.data[self.start:self.start+self.batch_size]

        for vid, anno in batch_sample:
            cap = cv2.VideoCapture(os.path.join(self.video_prefix, vid) + '.mp4')
            for ix, (cur_start, cur_end) in enumerate(anno['timestamps']):
                if cur_start > cur_end:
                    cur_start, cur_end = cur_end, cur_start

                anno['timestamps'][ix] = (cur_start, cur_end)

            frames = self.get_frames_data(cap, anno['timestamps'])

            # timestamps list of (start_ix,end_ix)
            cur_batch.timestamps.append(anno['timestamps'])

            cur_batch.vid.append(vid)
            cur_batch.frames.append(frames)

        self.start += self.batch_size

        return cur_batch

class DatasetBatcher:
    def __init__(self, data, type, feats_dir, word2ix, batch_size=1):
        self.data = data
        self.type = type
        self.feats_dir = feats_dir
        self.batch_size = batch_size
        self.word2ix = word2ix

        self.Batch = collections.namedtuple('Batch',['vid','duration','feats','sentences','word_id','timestamps','feats_len'])
        self.start = 0
        self.epoch = 0

    def prepare_feed_data(self):
        _input_data_list = []
        _dec_in_list = []
        _dec_target_list = []

        batch = self.next_batch()

        _input_data_list.append(batch.vid)

        n_events = len(batch.feats)

        for j in range(n_events):
            cur_word_id = batch.word_id[j][0]

            _input_data = batch.feats[j]
            _dec_in = ([FLAGS.GO] + cur_word_id)[:-1]

            # replace first pad with eos
            first_pad = cur_word_id.index(FLAGS.PAD)
            cur_word_id[first_pad] = FLAGS.EOS

            _dec_target = cur_word_id

            _input_data_list.append(_input_data)
            _dec_in_list.append([_dec_in])
            _dec_target_list.append([_dec_target])

        return _input_data_list, _dec_in_list, _dec_target_list, batch.timestamps[0], batch.feats_len

    def next_batch(self):
        if self.start > len(self.data)-self.batch_size:
            if self.type=='train':
                shuffled = sorted(self.data,key=lambda x:np.random.rand())
                self.data = collections.OrderedDict(shuffled).items()
                print 'End of Epoch... Shuffle !!'
            self.start = 0
            self.epoch += 1

        cur_batch = self.Batch(vid=[], duration=[], feats=[], sentences=[], word_id=[], timestamps=[], feats_len=[])
        batch_sample = self.data[self.start:self.start+self.batch_size]

        feats_batch = []
        word_id_batch = [] # for padding multiple word_ids later ...
        for vid, anno in batch_sample:
            cur_feats = []
            for ix, (cur_start, cur_end) in enumerate(anno['timestamps']):
                if cur_start > cur_end:
                    cur_start, cur_end = cur_end, cur_start
                if cur_start == cur_end:
                    cur_end = cur_start + 1

                anno['timestamps'][ix] = (cur_start, cur_end)
                feats_path = os.path.join(self.feats_dir, vid, 'event-{}.npy'.format(ix))

                if os.path.exists(feats_path):
                    np_feats = np.load(feats_path).squeeze(axis=0)
                    cur_batch.feats_len.append(np_feats.shape[0])
                    cur_feats.append(np_feats)

            # timestamps list of (start_ix,end_ix)
            cur_batch.timestamps.append(anno['timestamps'])

            cur_batch.vid.append(vid)
            cur_batch.duration.append(anno['duration'])
            cur_batch.sentences.append([ ' '.join(t.tokenize(s.strip().lower())) for s in anno['sentences']])

            # feats
            feats_batch.append(cur_feats)

            # word_id
            word_id_batch.append([[self.word2ix.get(word) \
                                   for word in t.tokenize(cap.strip().lower()) ] \
                                  for cap in anno['sentences']])

        # feats padding
        max_split = max([len(feats_batch[i]) for i in range(self.batch_size)])
        max_len = max([feats_batch[i][j].shape[0] for i in range(self.batch_size) \
                       for j in range(max_split)])

        padded_feats = np.zeros([max_split, self.batch_size, max_len, FLAGS.input_size], dtype=np.float32)

        for i in range(max_split):
            for j in range(self.batch_size):
                padded_feats[i, j, :feats_batch[j][i].shape[0]] = feats_batch[j][i]

        cur_batch.feats.extend(padded_feats)

        # word padding
        max_split = max([len(word_id_batch[i]) for i in range(self.batch_size)])
        max_len = max([len(word_id_batch[i][j]) for i in range(self.batch_size) \
                       for j in range(max_split)])

        padded_word_id = np.zeros([max_split, self.batch_size, max_len+1], dtype=np.int32)

        for i in range(max_split):
            for j in range(self.batch_size):
                padded_word_id[i, j, :len(word_id_batch[j][i])] = word_id_batch[j][i]

        cur_batch.word_id.extend(padded_word_id.tolist())

        self.start += self.batch_size

        return cur_batch